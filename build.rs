// Copyright 2018-2020 the Deno authors. All rights reserved. MIT license.

use deno_core::JsRuntime;
use deno_core::RuntimeOptions;

use std::env;
use std::path::PathBuf;

fn main() {
    let lambda_extension = deno_core::Extension::builder()
        .js(deno_core::include_js_files!(
          prefix "deno:lambda",
          "src/jsapi.js",
        ))
        .build();

    let o = PathBuf::from(env::var_os("OUT_DIR").unwrap());
    let snapshot_path = o.join("LAMBDA_SNAPSHOT.bin");
    let options = RuntimeOptions {
        will_snapshot: true,
        extensions: vec![deno_console::init(), lambda_extension],
        ..Default::default()
    };
    let mut isolate = JsRuntime::new(options);

    let snapshot = isolate.snapshot();
    let snapshot_slice: &[u8] = &*snapshot;
    std::fs::write(&snapshot_path, snapshot_slice).unwrap();
}
