// mandlebrot set in near lambda calculus.
const tup = a => b => f => f(a)(b);
const True = a => b => a;
const False = a => b => b;
const not = a => a(False)(True);
const or = a => b => a(a)(b);
const and = a => b => a(b)(a);
const fst = p => p(True);
const snd = p => p(False);
const Y = f => (y => y(y))(y => f(x => y(y)(x)));
// N Operations
const zero = f => v => v;
const succ = n => f => v => f(n(f)(v));
const mult = a => b => v => a(b(v));
const idk = p => tup(snd(p))(succ(snd(p)));
const pred = n => fst(n(idk)(tup(zero)(zero)));
const minus = a => b => b(pred)(a);
const isz = n => n(True(False))(True);
const lte = a => b => isz(minus(a)(b));
const eq = a => b => and(lte(b)(a))(lte(a)(b))
const div = Y(self => a => b => lte(succ(a))(b)(() => zero)(() => succ(self(minus(a)(b))(b)))());
// Z Operations
const NtoZ = n => tup(False)(n);
const zeroZ = NtoZ(zero);
const succZ = z => isz(snd(z))(() => tup(False)(succ(zero)))(() => tup(fst(z))(fst(z)(pred)(succ)(snd(z))))();
const negate = z => tup(not(fst(z)))(snd(z));
const predZ = z => negate(succZ(negate(z)));
const addZ = a => b => snd(b)(fst(b)(predZ)(succZ))(a);
const multZ = a => b => tup(not(fst(a)(fst(b))(not(fst(b)))))(mult(snd(a))(snd(b)));
const eqZ = a => b => fst(a)(fst(b))(not(fst(b)))(eq(snd(a))(snd(b)))(False);
// Q Opertions
const ZtoQ = z => tup(z)(succZ(zeroZ));
const zeroQ = ZtoQ(zeroZ);
const multQ = a => b => or(isz(snd(fst(a))))(isz(snd(fst(b))))(() => zeroQ)(() => tup(multZ(fst(a))(fst(b)))(multZ(snd(a))(snd(b))))();
const addQ = a => b => eqZ(snd(a))(snd(b))(() => tup(addZ(fst(a))(fst(b)))(snd(a)))(() => tup(addZ(multZ(fst(a))(snd(b)))(multZ(fst(b))(snd(a))))(multZ(snd(a))(snd(b))))();
const negateQ = q => tup(negate(fst(q)))(snd(q));
// C Operations
const zeroC = tup(zeroQ)(zeroQ);
const addC = a => b => tup(addQ(fst(a))(fst(b)))(addQ(snd(a))(snd(b)));
const multC = a => b => tup(addQ(multQ(fst(a))(fst(b)))(negateQ(multQ(snd(a))(snd(b)))))(addQ(multQ(fst(a))(snd(b)))(multQ(snd(a))(fst(b))));
// mandlebrot
const step = c => z => addC(multC(z)(z))(c);
const escapedQ = q => not(lte(div(snd(fst(q)))(snd(snd(q))))(succ(zero)));
const escapedC = c => or(escapedQ(fst(c)))(escapedQ(snd(c)));
const numSteps = succ(zero); // max recursion depth = 2
const sim = c => Y(self => s => z => isz(s)(() => True)(() => escapedC(z)(() => False)(() => self(pred(s))(step(c)(z)))())())(numSteps)(c);
const resolutionStep = tup(succZ(zeroZ))(NtoZ(numSteps)); // step = 1 / 2;
const stepr = tup(resolutionStep)(zeroQ);
const stepi = tup(zeroQ)(negateQ(resolutionStep));
const start = tup(ZtoQ(predZ(predZ(zeroZ))))(ZtoQ(succZ(zeroZ)));
const row = c => Y(self => c => tup(sim(c))(() => self(addC(c)(stepr))))(c);
const grid = Y(self => c => tup(row(c))(() => self(addC(c)(stepi))))(start);

const fifth = v => fst(snd(snd(snd(snd(v)))));
console.log(fifth(fifth(grid)).toString());

// Not lambda calculus
const take = (n, p) => n === 0 ? [] : [fst(p), ...take(n - 1, snd(p)())];
console.log(take(4, grid).map(r => take(6, r).map(b => b('#')('.')).join('')).join('\n'));
