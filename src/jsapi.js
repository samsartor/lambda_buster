((window) => {
    const core = window.Deno.core;

    let TABLE = []

    class Lambda extends Function {
        constructor(id) {
            const lambda = (arg) => {
                let arg_id;
                if (arg instanceof Lambda) {
                    arg_id = arg._id;
                } else {
                    arg_id = core.opSync('lambda_box', TABLE.length);
                    TABLE[TABLE.length] = arg;
                }
                return new Lambda(core.opSync('lambda_apply', [id, arg_id]));
            };
            lambda._id = id;
            return Object.setPrototypeOf(lambda, new.target.prototype);
        }

        toString() {
            core.opSync('lambda_normalize', this._id);
            let box = core.opSync('lambda_unbox', this._id);
            if (box == null) {
                return core.opSync('lambda_format', this._id);
            } else {
                return TABLE[box].toString();
            }
        }
    }

    window.Lambda = Lambda;
})(this);
