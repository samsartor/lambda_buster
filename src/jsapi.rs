use crate::lambda::{self, Rex};
use anyhow::Error;
use deno_core::{
    include_js_files, op_sync, Extension, JsRuntime, OpState, Resource, ResourceTable,
};
use std::collections::HashMap;
use swc_common::{Span, Spanned};
use swc_ecma_ast::{Expr, ExprOrSpread, Lit, NewExpr, Script};
use swc_ecma_utils::quote_ident;
use swc_ecma_visit::{visit_mut_expr, visit_mut_script, VisitMut};

impl Resource for Rex {}

pub struct Transform<'a> {
    pub exprs: &'a HashMap<Span, Rex>,
    pub table: &'a mut ResourceTable,
}

impl<'a> VisitMut for Transform<'a> {
    fn visit_mut_expr(&mut self, expr: &mut Expr) {
        if let Some(rex) = self.exprs.get(&expr.span()) {
            let id = self.table.add(rex.clone()) as usize;
            *expr = Expr::New(NewExpr {
                span: Default::default(),
                callee: Box::new(Expr::Ident(quote_ident!("Lambda"))),
                args: Some(vec![ExprOrSpread {
                    spread: None,
                    expr: Box::new(Expr::Lit(Lit::Num(id.into()))),
                }]),
                type_args: None,
            });
        } else {
            visit_mut_expr(self, expr);
        }
    }
}

pub fn transform(script: &mut Script, exprs: &HashMap<Span, Rex>, rt: &mut JsRuntime) {
    let state = rt.op_state();
    let mut state = state.borrow_mut();
    let mut t = Transform {
        exprs,
        table: &mut state.resource_table,
    };
    visit_mut_script(&mut t, script);
}

fn op_box(OpState { resource_table, .. }: &mut OpState, index: usize, _: ()) -> Result<u32, Error> {
    let result: Rex = lambda::Box(index).into();
    Ok(resource_table.add(result))
}

fn op_apply(
    OpState { resource_table, .. }: &mut OpState,
    (callee, arg): (u32, u32),
    _: (),
) -> Result<u32, Error> {
    let callee = Rex::clone(&*resource_table.get(callee)?);
    let arg = Rex::clone(&*resource_table.get(arg)?);
    let result: Rex = lambda::App(callee, arg).into();
    Ok(resource_table.add(result))
}

fn op_unbox(
    OpState { resource_table, .. }: &mut OpState,
    obj: u32,
    _: (),
) -> Result<Option<usize>, Error> {
    let obj = resource_table.get::<Rex>(obj)?;
    let owner = lambda::Owner::new();
    if let lambda::Box(index) = (&*obj).as_ref(&owner) {
        Ok(Some(*index))
    } else {
        Ok(None)
    }
}

fn op_format(
    OpState { resource_table, .. }: &mut OpState,
    obj: u32,
    _: (),
) -> Result<String, Error> {
    Ok(resource_table.get::<Rex>(obj)?.to_string())
}

fn op_normalize(
    OpState { resource_table, .. }: &mut OpState,
    obj: u32,
    _: (),
) -> Result<(), Error> {
    let mut owner = lambda::Owner::new();
    lambda::normalize(&mut owner, Rex::clone(&*resource_table.get::<Rex>(obj)?));
    Ok(())
}

pub fn init() -> Extension {
    Extension::builder()
        .js(include_js_files!(
            prefix "deno:lambda", "src/jsapi.js",
        ))
        .ops(vec![
            ("lambda_box", op_sync(op_box)),
            ("lambda_apply", op_sync(op_apply)),
            ("lambda_unbox", op_sync(op_unbox)),
            ("lambda_format", op_sync(op_format)),
            ("lambda_normalize", op_sync(op_normalize)),
        ])
        .build()
}
