use crate::lambda::{App, Box, Lam, Rex, Var};
use anyhow::Error;
use std::collections::HashMap;
use std::path::Path;
use std::process::exit;
use std::rc::Rc;
use swc_common::errors::{ColorConfig, Handler};
use swc_common::{SourceMap, Span, Spanned};
use swc_ecma_ast::{BlockStmtOrExpr, Decl, Expr, Id, Script, Stmt, VarDeclKind, VarDeclarator};
use swc_ecma_codegen::{text_writer::JsWriter, Emitter};
use swc_ecma_parser::Parser;
use swc_ecma_parser::StringInput;
use swc_ecma_utils::ident::IdentLike;

pub fn read_input(path: &Path, cm: Rc<SourceMap>) -> Result<Script, Error> {
    let file = cm.load_file(path)?;
    match Parser::new(Default::default(), StringInput::from(&*file), None).parse_script() {
        Ok(script) => Ok(script),
        Err(error) => {
            error
                .into_diagnostic(&Handler::with_tty_emitter(
                    ColorConfig::Auto,
                    false,
                    false,
                    Some(cm.clone()),
                ))
                .emit();
            exit(1)
        }
    }
}

pub fn write_input(script: &Script, cm: Rc<SourceMap>) -> Result<String, Error> {
    let mut out = Vec::new();
    Emitter {
        cfg: Default::default(),
        cm: cm.clone(),
        comments: None,
        wr: JsWriter::new(cm, "\n", &mut out, None),
    }
    .emit_script(script)?;
    Ok(String::from_utf8(out).unwrap())
}

#[derive(Default)]
pub struct Converter {
    stack: Vec<Option<Id>>,
    pub refs: HashMap<Id, Rex>,
    pub exprs: HashMap<Span, Rex>,
}

impl<'id> Converter {
    pub fn lambda<O>(&mut self, arg: Option<Id>, func: impl FnOnce(&mut Self) -> O) -> O {
        self.stack.push(arg);
        let out = func(self);
        self.stack.pop();
        out
    }

    fn expr_(&mut self, expr: &Expr) -> Option<Rex> {
        Some(match expr {
            Expr::Call(call) => {
                let callee = self.expr(call.callee.as_expr()?)?;
                match call.args.len() {
                    0 => callee,
                    1 => App(callee, self.expr(&call.args[0].expr)?).into(),
                    _ => return None,
                }
            }
            Expr::Arrow(arrow) => {
                let id = match arrow.params.len() {
                    0 => None,
                    1 => Some(arrow.params[0].as_ident()?.to_id()),
                    _ => return None,
                };
                let body = self.lambda(id.clone(), |this| match &arrow.body {
                    BlockStmtOrExpr::BlockStmt(block) => this.stmts(&block.stmts),
                    BlockStmtOrExpr::Expr(expr) => this.expr(expr),
                });

                if let Some(id) = id {
                    Lam(body?, id).into()
                } else {
                    body?
                }
            }
            Expr::Ident(ident) => {
                let id = ident.to_id();
                if let Some(refed) = self.refs.get(&id) {
                    refed.clone()
                } else {
                    Var(id).into()
                }
            }
            Expr::Paren(expr) => self.expr(&expr.expr)?,
            _ => return None,
        })
    }

    pub fn expr(&mut self, expr: &Expr) -> Option<Rex> {
        let out = self.expr_(expr);
        if let Some(out) = out.clone() {
            self.exprs.insert(expr.span(), out);
        }
        out
    }

    pub fn decl(&mut self, var: &VarDeclarator) -> Option<()> {
        let id = var.name.as_ident()?.to_id();
        let body = self.expr(var.init.as_ref()?)?;
        self.refs.insert(id, body);
        Some(())
    }

    pub fn stmt(&mut self, stmt: &Stmt) -> Option<Rex> {
        match stmt {
            Stmt::Block(block) => self.stmts(&block.stmts),
            Stmt::Return(ret) => self.expr(ret.arg.as_ref()?),
            Stmt::Decl(Decl::Var(vars)) => {
                if vars.kind != VarDeclKind::Const {
                    return None;
                }
                for var in &vars.decls {
                    self.decl(var);
                }
                None
            }
            _ => None,
        }
    }

    pub fn stmts(&mut self, stmts: &[Stmt]) -> Option<Rex> {
        let mut ret = None;
        for stmt in stmts {
            ret = ret.or(self.stmt(stmt));
        }
        ret
    }
}

pub fn convert(script: &Script) -> Converter {
    let mut c = Converter::default();
    c.stmts(&script.body);
    c
}
