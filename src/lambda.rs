use qcell::{TCell, TCellOwner};
use std::{
    collections::HashMap,
    fmt, hash,
    mem::replace,
    rc::Rc,
};
use swc_ecma_ast::Id;

pub struct Lock;
pub type Owner = TCellOwner<Lock>;

#[derive(Clone)]
pub enum Ex {
    /// Opaque object
    Box(usize),
    /// Abstraction (λ0)
    Lam(Rex, Id),
    /// Application (0 1)
    App(Rex, Rex),
    /// Bound variable (https://en.wikipedia.org/wiki/De_Bruijn_index)
    Var(Id),
}

pub use Ex::*;

#[derive(Clone)]
pub struct Rex(Rc<TCell<Lock, Ex>>);

impl From<Ex> for Rex {
    fn from(ex: Ex) -> Self {
        Rex(Rc::new(TCell::new(ex)))
    }
}

impl Rex {
    pub fn as_ref<'a>(&'a self, owner: &'a Owner) -> &'a Ex {
        self.0.ro(owner)
    }

    pub fn as_mut<'a>(&'a self, owner: &'a mut Owner) -> &'a mut Ex {
        self.0.rw(owner)
    }

    fn fmt(&self, o: &Owner, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.as_ref(o) {
            Box(i) => write!(f, "${i}"),
            Lam(x, id) => {
                write!(f, "λ{}.", id.0)?;
                x.fmt(o, f)
            }
            App(x, y) => {
                write!(f, "(")?;
                x.fmt(o, f)?;
                write!(f, " ")?;
                y.fmt(o, f)?;
                write!(f, ")")
            }
            Var(i) => write!(f, "{}", i.0),
        }
    }
}

impl PartialEq for Rex {
    fn eq(&self, other: &Self) -> bool {
        Rc::ptr_eq(&self.0, &other.0)
    }
}

impl Eq for Rex {}

impl hash::Hash for Rex {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        (Rc::as_ptr(&self.0) as usize).hash(state);
    }
}

impl fmt::Display for Rex {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.fmt(&Owner::new(), f)
    }
}

struct WithOwner<'a>(&'a Rex, &'a Owner);

impl<'a> fmt::Display for WithOwner<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(&self.1, f)
    }
}

fn substituted<'c, 'r>(
    owner: &'r Owner,
    cache: &'c mut HashMap<&'r Rex, Rex>,
    ex: &'r Rex,
    var: &Id,
    with: &Rex,
) -> Rex {
    if let Some(out) = cache.get(&ex) {
        return out.clone();
    }

    let out = (|| {
        match ex.as_ref(owner) {
            Lam(_, id) if id == var => (),
            Lam(x1, id) => {
                let x2 = substituted(owner, cache, x1, var, with);
                if x1 != &x2 {
                    return Lam(x2, id.clone()).into();
                }
            }
            App(x1, y1) => {
                let x2 = substituted(owner, cache, x1, var, with);
                let y2 = substituted(owner, cache, y1, var, with);
                if x1 != &x2 || y1 != &y2 {
                    return App(x2, y2).into();
                }
            }
            Var(id) if var == id => return with.clone(),
            Var(_) | Box(_) => (),
        }

        ex.clone()
    })();

    cache.insert(ex, out.clone());
    out
}

pub fn contract(owner: &mut Owner, ex: &Rex) -> bool {
    if let App(callee, with) = ex.as_ref(owner) {
        if let Lam(body, var) = callee.as_ref(owner) {
            let new = substituted(owner, &mut HashMap::new(), body, &var, with);
            // println!("{} -> {}", WithOwner(ex, owner), WithOwner(&new, owner));
            let new = new.as_ref(owner).clone();
            *ex.as_mut(owner) = new;
            return true;
        }
    }
    false
}

pub fn weak_normalize(owner: &mut Owner, ex: Rex) {
    let mut stack = Vec::new();
    let mut head;
    if let App(callee, _) = ex.as_ref(owner) {
        head = callee.clone();
        stack.push(ex);
    } else {
        return;
    }

    loop {
        // Build a stack of apps going right to left
        while let App(callee, _) = head.as_ref(owner) {
            let callee = callee.clone();
            stack.push(replace(&mut head, callee.clone()));
        }

        // Contract the leftmost app
        loop {
            head = match stack.pop() {
                Some(head) => head,
                None => return,
            };
            if contract(owner, &head) {
                break;
            }
        }
    }
}

pub fn normalize(owner: &mut Owner, ex: Rex) {
    weak_normalize(owner, ex.clone());
    match ex.as_ref(owner) {
        Lam(body, _) => {
            let body = body.clone();
            normalize(owner, body)
        }
        App(a, b) => {
            let a = a.clone();
            let b = b.clone();
            normalize(owner, a);
            normalize(owner, b);
        }
        Box(_) | Var(_) => (),
    }
}
