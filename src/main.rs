use anyhow::{bail, Context, Error};
use deno_core::{located_script_name, JsRuntime, RuntimeOptions, Snapshot};
use std::rc::Rc;
use swc_common::SourceMap;

mod jsapi;
mod jsinput;
mod lambda;

static LAMBDA_SNAPSHOT: &[u8] = include_bytes!(concat!(env!("OUT_DIR"), "/LAMBDA_SNAPSHOT.bin"));

#[derive(argh::FromArgs)]
/// Beta-reduction at scale!
struct LambdaBuster {
    #[argh(positional)]
    source: std::path::PathBuf,
    #[argh(option)]
    /// the variable name to normalize and print
    main: Option<String>,
}

fn main() -> Result<(), Error> {
    let LambdaBuster { source, main } = argh::from_env();
    env_logger::init();

    let cm: Rc<SourceMap> = Default::default();
    let mut script = jsinput::read_input(&source, cm.clone())
        .with_context(|| format!("could not read {source:?}"))?;
    let script_name = source.file_name().unwrap().to_string_lossy();

    let exs = jsinput::convert(&script);

    if let Some(main) = main {
        for ((name, _), ex) in exs.refs {
            if name == main {
                lambda::normalize(&mut lambda::Owner::new(), ex.clone());
                println!("{ex}");
                return Ok(());
            }
        }
        bail!("could not find {}", main);
    }

    let mut rt = JsRuntime::new(RuntimeOptions {
        extensions: vec![jsapi::init(), deno_console::init()],
        startup_snapshot: Some(Snapshot::Static(LAMBDA_SNAPSHOT)),
        ..Default::default()
    });

    jsapi::transform(&mut script, &exs.exprs, &mut rt);
    let script = jsinput::write_input(&script, cm)?;

    rt.execute_script(
        &located_script_name!(),
        "this.console = new this.__bootstrap.console.Console(Deno.core.print);",
    )?;
    rt.execute_script(&*script_name, &script)?;

    Ok(())
}
