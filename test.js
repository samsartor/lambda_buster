// \a.\b. a b
const cat = a => b => a(b);
// (\a1.\b1. a1 b1)(\a2.\b2. a2 b2)
// => \b1. (\a2.\b2. a2 b2) b1
// => \b1. (\b2. b1 b2)
const nest = cat(cat);
// (\b1.\b2. b1 b2) x y
// \x.\y. x y
const fixed = x => y => nest(x)(y)
